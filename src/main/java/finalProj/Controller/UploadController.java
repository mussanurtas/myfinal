package finalProj.Controller;

import finalProj.Model.FileForUpload;
import finalProj.Repo.FileRepository;
import org.apache.tomcat.util.http.fileupload.FileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping(value = "/file")
public class UploadController {

    private static String UPLOAD_DIR = "/home/kairatawer/web/forFinal/upload/";

    @Autowired
    private FileRepository fileRepo;

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping(headers=("content-type=multipart/*"),value = "", method = RequestMethod.PUT)
    public ResponseEntity imageUpload(@RequestParam("image") MultipartFile file) throws Exception {
        String response = "";
        try {

            String extension = getExtensionOfFile(file.getOriginalFilename());
            if(!extension.equals("txt")) throw new Exception("No .txt format");

            byte[] bytes = file.getBytes();
            Date date = new Date();

            FileForUpload newFile = new FileForUpload();
            String pathToFile = "";
            InputStream is = file.getInputStream();
            String text = fileToString(is);

            if(!text.toLowerCase().contains("spam")) {
                String filename = UUID.randomUUID().toString();
                File fileInLocal = new File(UPLOAD_DIR + dateFormat.format(date));
                if(!fileInLocal.exists()) {
                    fileInLocal.mkdir();
                }
                pathToFile = UPLOAD_DIR + dateFormat.format(date) + "/" + filename + ".txt";
                newFile.setFileName(file.getOriginalFilename());
                newFile.setFilePath(pathToFile);
                response = "file saved";
            } else {
                String filename = UUID.randomUUID().toString();
                File fileInLocal = new File(UPLOAD_DIR + "spam");
                if(!fileInLocal.exists()) {
                    fileInLocal.mkdir();
                }
                pathToFile = UPLOAD_DIR + "spam/" + filename + ".txt";
                newFile.setFileName(file.getOriginalFilename());
                newFile.setFilePath(pathToFile);
                response = "file saved in spam dir";
            }

            Path path = Paths.get(pathToFile);
            Files.write(path, bytes);

            newFile.setDate(date);
            newFile.setBytes(bytes);
            fileRepo.save(newFile);
        } catch (IOException e) {
            return new ResponseEntity("Bad Request",HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(response,HttpStatus.ACCEPTED);
    }


    @RequestMapping(value = "/byDate", method = RequestMethod.GET)
    public @ResponseBody ArrayList<FileForUpload> listByDate(@RequestParam("date") String date) {
        ArrayList<FileForUpload> filtered = new ArrayList<>();
        Iterable<FileForUpload> all = fileRepo.findAll();
        for(FileForUpload f : all) {
            if(f.getDate().toString().startsWith(date)) filtered.add(f);
        }
        return filtered;
    }

    @RequestMapping(value = "/spam", method = RequestMethod.GET)
    public @ResponseBody ArrayList<FileForUpload> listSpams() {
        ArrayList<FileForUpload> spams = new ArrayList<>();
        Iterable<FileForUpload> all = fileRepo.findAll();
        for(FileForUpload f : all) {
            File file = new File(f.getFilePath());
            String path = file.getParent();
            if(path.substring(path.lastIndexOf("/")+1,path.length()).toString().equals("spam")) {
                spams.add(f);
            }
        }
        return spams;
    }

    @RequestMapping(value = "/deleteSpam", method = RequestMethod.DELETE)
    public void deleteSpams() {
        Iterable<FileForUpload> all = fileRepo.findAll();
        for(FileForUpload f : all) {
            File file = new File(f.getFilePath());
            String path = file.getParent();
            if(path.substring(path.lastIndexOf("/")+1,path.length()).toString().equals("spam")) {
                File fileInLocal = new File(f.getFilePath());
                if(fileInLocal.exists() && !fileInLocal.isDirectory()) {
                    fileInLocal.delete();
                }
                fileRepo.delete(f);
            }
        }
    }








    public String fileToString(InputStream is) throws IOException {
        int ch;
        StringBuilder sb = new StringBuilder();
        while((ch = is.read()) != -1) {
            sb.append((char) ch);
        }
        return sb.toString();
    }

    public String getExtensionOfFile(String fileName) {
        String extension = "";
        int i = fileName.lastIndexOf('.');
        if (i >= 0) {
            extension = fileName.substring(i+1);
        }
        return extension;
    }
}
