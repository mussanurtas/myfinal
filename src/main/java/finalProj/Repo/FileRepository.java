package finalProj.Repo;

import finalProj.Model.FileForUpload;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by kairatawer on 4/29/17.
 */

public interface FileRepository extends CrudRepository<FileForUpload,Long> {
    FileForUpload findByName(String name);
    Iterable<FileForUpload> findByDate(Date date);
}
